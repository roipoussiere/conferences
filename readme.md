Conférences
===========

Source des supports de présentation de mes conférences.

## Utilisation

La plupart de ces diapos sont rédigés en markdown en utilisant [Marp](https://yhatt.github.io/marp/). Vous devez installer ce logiciel pour re-générer les fichiers pdf.

Les sources contiennent également en commentaire un résumé de ce que j'envisageais de dire à l'oral pour chaque diapo.

## Contenu

- [Téléphones libres](./telephones_libres) ;
- [Présentation d'ActivityPub](./presentation_activitypub) ;
- [PeerTube, une plateforme de partage de vidéo libre et décentralisée](./peertube-cdl2018) ;
- [Construire un Internet éthique](./internet_ethique-insa) (co-réalisé avec Numahell)

## Licence

Tous ces diapos sont publiés sous licence [Creative Commons Attribution](https://creativecommons.org/licenses/by/2.0/).

Les images contenues dans ces diapos appartiennent à leurs auteurs respectifs. Voir les sources des diapos pour connaitre les licences sous lesquelles elles sont partagées.
