<!-- Please download Marp https://yhatt.github.io/marp/ to generate pdf document. -->

<style>
.slide {
  background-image: url("images/fond.jpg") !important;
  background-size: cover;
}
.slide h4 {
  color: gray;
  font-size: large !important;
  font-style: italic;
}
</style>

<!-- $theme: gaia -->
<!-- $size: 16:9 -->
<!-- footer: Nathanaël, pour Framasoft - 17 novembre, Capitole du Libre 2018 -->





# PeerTube

## Une plateforme de partage vidéo
## libre et décentralisée

---

<!-- page_number: true -->

# PeerTube

1. C'est quoi le problème avec Youtube ? <!-- 10mn -->
2. Créons une alternative <!-- 20mn -->
3. Où on en est aujourd'hui ? <!-- 10mn -->
4. Questions <!-- 10mn -->





---
# C'est quoi le problème avec YouTube ?





---
#### 1. C'est quoi le problème avec YouTube ?

## Une plateforme centralisée

- but : :moneybag: :moneybag: :moneybag:
- modèle économique : publicité
- obéit à l'économie de l'attention :eyes:
- 2 objectifs :
    - :arrow_right: attirer l'attention  <!-- d'un maximum de personnes -->
    - :arrow_right: maintenir cette attention

<!-- tue la création de contenu car obéit à un modèle capitaliste, les créateurs s'adaptent à la plateforme -->


---
#### 1. C'est quoi le problème avec YouTube ?
## Comment *attirer* l'attention (1/2)

Grâce aux recommendations et l'algorithme magique ! :innocent:

:arrow_right: encourage les contenus pièges à clics

![58% center](images/videos_debiles_genial.png)
<!-- dans le cas de black-mirror, ce qu'on fournit c'est de l'nrj-->
<!-- dans le cas de youtube, ce qu'on fournit c'est du temps de cerveau dispo pour regarder les publicités, au profit dezs vrais clients de Youtube qui sont les annonceurs publicitaires-->

---
#### 1. C'est quoi le problème avec YouTube ?
## Comment *attirer* l'attention (2/2)

Grâce aux recommendations et l'algorithme magique ! :innocent:

:arrow_right: encourage les contenus provoquant l'indignation (fake-news)

![72.5% center](images/terre_plate.png)


---
#### 1. C'est quoi le problème avec YouTube ?
## Comment *maintenir* cette attention ?

Ergonomie pensée pour nous enfermer dans Youtube

![165%](images/autoplay.png) ![75%](images/cloche_youtube.png)

<!-- cf. Tristan Harris, Time well spent -->
<!-- plutôt que des utilisateurs qui contrôlent un logiciel, c'est logiciel qui contrôle utilisateur -->
<!-- ça y est tu as fini ? ok bon, maintenant regarde cette vidéo -->


---
#### 1. C'est quoi le problème avec YouTube ?
## Le profilage des données

<!-- bien sur on ne se contente pas de nous placarder de la pub, faut aussi qu'elle nous corresponde -->

![90% center](images/pub_youtube.jpg)

<!-- on peut dire ce qu'on veut de Google, mais bon au moins ils sont honêtes -->


---
#### 1. C'est quoi le problème avec YouTube ?
## Comment luter contre le monopole

Trouver des alternatives...

- Vimeo ?
- DailyMotion ?

![150% center](images/thinking_face.png)

<!-- le pb c'est pas Youtube, c'est l'économie de l'attention -->





---
# Créons une alternative





---
#### 2. Créons une alternative
## Quelques chiffres sur Youtube

<!-- Framasoft  -->
<!-- Dailymotion racheté pour 265 millions d'euros il y a 3 ans.
Donc bon ça nous aurait bien plus à Framasoft mais on avait pas le budget (bon à l'époque hein bien sur).
-->

Le temps de ma conférence <small>(chiffres février 2017)</small> :
- 840 jours de vidéos uploadées sur Youtube ;
- 4000 années de vidéos vues.

Ça nécessite :
- beaucoup de **bande-passante**. <!-- c'est pas pour rien que Google se met à poser des cables -->
- beaucoup de **stockage** ; <!-- des années de vidéos dans d'imenses datacenters -->
- beaucoup de **temps humain**. <!-- pour administrer les serveurs et modérer les vidéos, et dév. le logiciel -->


---
#### 2. Créons une alternative
## Envisager un autre fonctionnement

... basé sur la **contribution** :

- beaucoup de **bande-passante** : distribuons-la !
- beaucoup de **stockage** : décentralisons-le !
- beaucoup de **temps humain** : déléguons-les !

![18% center](images/contributopia.jpg)


---
#### 2. Créons une alternative
## Distribuer la bande passante

En utilisant le partage pair à pair (BitTorrent)

![40% center](images/client_serveur_vs_p2p.png)


---
#### 2. Créons une alternative
## Distribuer la bande passante

Avec WebTorrent

![40%](images/peertube_p2p.png) ![30%](images/webtorrent.png)


---
#### 2. Créons une alternative
## Décentraliser le stockage

![48% center](images/federation.png)


---
#### 2. Créons une alternative
## Fédérer les services

![48% center](images/federation_avec_masto.png)


---
#### 2. Créons une alternative
## Rester neutre

Encourager la création de contenu quelque-soit sa valeur monétaire.

- limiter l'influence de PeerTube sur les auteurs ;
- limiter la responsabilité des admins aux tâches de modération.

![](images/star.png)

<!-- L'idée est d'encourager la création de contenu quelque soit sa valeur monétaire.
On veut rester neutres en limitant l'influence de PeerTube sur les auteurs. La responsabilité des admins d'instances doit être uniquement de l'ordre de la modération.
-->



---
# Où on en est aujourd'hui ?





---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet

![center](images/history.png)


---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet
- :wave: **juillet 2017** : Pyg rencontre Chocobozzz aux RMLL

![60% center](images/rmll2017.png)


---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet
- :wave: **juillet 2017** : Pyg rencontre Chocobozzz aux RMLL
- :sunglasses: **octobre 2017** : Framasoft embauche Chocobozzz

![96% center](images/services.jpg)

---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet
- :wave: **juillet 2017** : Pyg rencontre Chocobozzz aux RMLL
- :sunglasses: **octobre 2017** : Framasoft embauche Chocobozzz
- :kissing_heart: **novembre 2017** : appel aux dons dans la francophonie

![100% center](images/barre_progression.png)


---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet
- :wave: **juillet 2017** : Pyg rencontre Chocobozzz aux RMLL
- :sunglasses: **octobre 2017** : Framasoft embauche Chocobozzz
- :kissing_heart: **novembre 2017** : appel aux dons dans la francophonie
- :heart_eyes: **mars 2018** : sortie de la beta publique

<!-- ~100 instances -->

![35% center](images/beta.png)


---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet
- :wave: **juillet 2017** : Pyg rencontre Chocobozzz aux RMLL
- :sunglasses: **octobre 2017** : Framasoft embauche Chocobozzz
- :kissing_heart: **novembre 2017** : appel aux dons dans la francophonie
- :heart_eyes: **mars 2018** : sortie de la beta publique
- :tada: **juin 2018** : campagne de financement participatif

![32% center](images/campagne.png)


<!-- +50k€ = salaire Chocobozzz assuré jusqu’à fin de l’année -->


---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet
- :wave: **juillet 2017** : Pyg rencontre Chocobozzz aux RMLL
- :sunglasses: **octobre 2017** : Framasoft embauche Chocobozzz
- :kissing_heart: **novembre 2017** : appel aux dons dans la francophonie
- :heart_eyes: **mars 2018** : sortie de la beta publique
- :tada: **juin 2018** : campagne de financement participatif
- :scream: **septembre 2018** : Chocobozzz en CDI

---
#### Où on en est aujourd'hui ?
## Rappel des épisodes précédents

- :sparkles: **octobre 2015** : Chocobozzz commence le projet
- :wave: **juillet 2017** : Pyg rencontre Chocobozzz aux RMLL
- :sunglasses: **octobre 2017** : Framasoft embauche Chocobozzz
- :kissing_heart: **novembre 2017** : appel aux dons dans la francophonie
- :heart_eyes: **mars 2018** : sortie de la beta publique
- :tada: **juin 2018** : campagne de financement participatif
- :scream: **septembre 2018** : Chocobozzz en CDI
- :fire: **octobre 2018** : sortie de la v1


---
#### Où on en est aujourd'hui ?
## Et ça ressemble à quoi ?

![64% center](images/demo_simple.png)


---
#### Où on en est aujourd'hui ?
## Et ça ressemble à quoi (coté admin) ?

![64% center](images/demo_admin.png)

---
#### Où on en est aujourd'hui ?
## Intégration avec Mastodon

![67%](images/masto.png) ![80%](images/pouvoir_fediverse.png)

---
#### Où on en est aujourd'hui ?
## Fonctionnalités de base

- lecture de vidéo avec WebTorrent
- fédération entre instances PeerTube
- s'interface avec Mastodon
- quotas et rôles <!-- nb utilisateurs, espace disque -> gouvernance -->
- transcodage
- peut fonctionner sur un petit serveur
- mode *Théâtre* et mode *Nuit*


---
#### Où on en est aujourd'hui ?
## Fonctionnalités financées par le crowdfunding

- sous-titrage
- abonnement aux chaines
- redondance des vidéos
- import depuis d'autres plateformes
- flux RSS
- internationalisation
- recherche avancée


---
#### Où on en est aujourd'hui ?
# Et si on continuait ? :crystal_ball:

<!-- perpectives pour la suite -->

- système de plugin
- statistiques
- améliorer outils d'importation
- améliorer outils de modération
- une appli mobile ?
- un client BitTorrent compatible ?


---
#### Où on en est aujourd'hui ?
## Qu'est ce qu'on a au menu ? :stuck_out_tongue:

![center](images/instances/datagueule.png)

https://peertube.datagueule.tv


---
#### Où on en est aujourd'hui ?
## Qu'est ce qu'on a au menu ? :stuck_out_tongue:

![center](images/instances/thinkerview.png)

https://thinkerview.video


---
#### Où on en est aujourd'hui ?
## Qu'est ce qu'on a au menu ? :stuck_out_tongue:

![center](images/instances/fontube.png)

https://fontube.fr


---
#### Où on en est aujourd'hui ?
## Qu'est ce qu'on a au menu ? :stuck_out_tongue:

![center](images/instances/blender.png)

https://video.blender.org


---
#### Où on en est aujourd'hui ?
## Qu'est ce qu'on a au menu ? :stuck_out_tongue:

![center](images/instances/skeptikon.png)

https://skeptikon.fr/


---
#### Où on en est aujourd'hui ?
## Qu'est ce qu'on a au menu ? :stuck_out_tongue:

![center](images/instances/framatube.png)

https://framatube.org


---
#### Où on en est aujourd'hui ?
## Quelques chiffres sur PeerTube :bar_chart:

![110% center](images/stats.png)

https://instances.joinpeertube.org


---
#### Où on en est aujourd'hui ?
# Envie de contribuer ?

- :tv: utiliser
- :speech_balloon: communiquer <!-- autour de vous, media sociaux, articles, conférences, ... -->
- :bulb: donnez votre avis <!-- forum, tickets GitHub -->
- :house: héberger une instance
- :symbols: traductions
- :arrow_heading_down: participer au code <!-- pull requests, revues de code, projets tierces -->
- :books: documentation
- :moneybag: dons

<!-- aller à l'atelier de contribution, salle B06-B07 -->


---
# Liens

##### Site : https://joinpeertube.org/fr/
##### Forum : https://framacolibri.org/c/peertube
##### Chat : #peertube sur chat.freenode.net:6697 / matrix.org
##### Code :  https://github.com/Chocobozzz/PeerTube
##### Soutenir :  https://soutenir.framasoft.org


---
# Remerciements

##### :heart: bénévoles Capitole du Libre :heart:
##### :heart: Interprètes LSF :heart:
##### :heart: Chocobozzz :heart:
##### :heart: Contributeurs PeerTube :heart:
##### :heart: Framasoft :heart:
##### :heart: Contributeurs campagnes de financement :heart:
##### :heart: David Revoy & Aryeom Han (ZeMarmot) :heart:


---
# Des questions ?