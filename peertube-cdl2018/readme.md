# PeerTube, une plateforme de partage de vidéo libre et décentralisée

Capitole du Libre 2018 - samedi 17 novembre

[![download pdf](https://img.shields.io/badge/pdf-download-blue.svg)](https://framagit.org/roipoussiere/conferences/raw/master/peertube-cdl2018/peertube-cdl2018.pdf?inline=false)
[![view source](https://img.shields.io/badge/source-view-orange.svg)](./peertube-cdl2018.md)
[![watch video](https://img.shields.io/badge/video-watch-red.svg)](https://www.youtube.com/watch?v=f7hymPTI3Oc)
[![event page](https://img.shields.io/badge/event%20page-go-green.svg)](https://2018.capitoledulibre.org/programme/#peertube-une-plateforme-de-partage-de-video-libre-)

![](./conf_light.png)

*Il est difficile aujourd’hui de construire une alternative libre à YouTube, car les coûts de stockage et de bande passante seraient trop importants pour une seule et même entité.*

*PeerTube est une application web développée par Framasoft essayant de répondre à ces deux contraintes :*

- *utiliser la fédération (via protocole ActivityPub) pour construire un index des vidéos présentes dans le fédiverse sans toutes les stocker ;*
- *utiliser le BitTorrent (via la bibliothèque WebTorrent) pour partager la bande passante entre les utilisateurs qui regardent une même vidéo.*

*Le but de cette conférence est de présenter le projet, ses différents aspects techniques ainsi que les futurs développements prévus.*

*Plus d'infos sur : https://joinpeertube.org/*
