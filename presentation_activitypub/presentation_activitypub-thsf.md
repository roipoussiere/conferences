<!-- $theme: gaia -->

# Comment réparer
# les médias sociaux ?

---

<!-- page_number: true -->

> Le principe même de communication, du et des langage(s) et de leur appropriation pour la création de communs.
<!-- C'était le thème du thsf 2018 -->

---

> Le principe même de communication, du et des langage(s) et de leur appropriation pour la création de communs.

1. Le principe même de **communication** ;
2. du et des **langage(s)** ;
3. et de leur **appropriation** ;
4. pour la création de **communs**.

---

## 1. Le principe même de communication

<!-- Facebook est un outil qui permet de communiquer. Moi quand je regarde des gens rester plus d'1h sur fb à scroller leur timeline, je me pose quand même des questions sur ce que c'est que le principe même de communication. Il est clair qu'elle est alterée. -->

Dans le cas de Facebook, Twitter, etc. altérée par :

  - **bruit** : publicités ciblées, contenus sponsorisés ;
  - **filtrage** : censure, classement fermé ;
  - **manipulation** : classement fermé, interface.

<!-- bruit au sens théorie de l'information -->
<!-- interface concue par une entreprise dont le but est de faire des profits en vendant notre temps de cerveau disponible -->

---

#### Solutions

- ~~ne rien faire~~ ;
- ne pas utiliser les médias sociaux ;
- utiliser des médias sociaux alternatifs :
	- en qui faire confiance ?

<!-- Ne faites confiance en personne, en aucun acteur central. Le seul moyen efficace de lutter contre la centralisation des données dans des imenses silos, c'est de faire des silos plus petits. -->

![60%](images/deleteFB.jpg)

---

## 2. du et des langage(s)

![center 15%](images/reseau-ap-1.svg)

---

## 2. du et des langage(s)

![center 15%](images/reseau-ap-2.svg)

---

## 2. du et des langage(s)

![center 15%](images/reseau-ap-3.svg)

---

## 3. et de leur appropriation ;

![center 15%](images/reseau-ap-4.svg)

<!-- Chacun peut s'approprier le langage comme il veut -->
<!-- demo ! -->

---

(pendant ce temps sur les médias sociaux centralisés…)

![center](images/reseaux-centralises.svg)

---

## 4. pour la création de communs.

> Une ressource (bien commun) *plus* les interactions sociales (**économiques**, **culturelles** et **politiques**) au sein de la communauté prenant soin de cette ressource. (lescommuns.org)

- **économie** : frais d'hébergement distribués ;
- contenus **culturels** (textes, musiques, images) ;
- **politique** liée aux CGU de chaque instance.

![30%](images/wema.png) ![62%](images/switter.png)

---

### Quelques projets fédérés

- micro-blogging : Mastodon ![20%](images/mastodon.svg) ou Pleroma
- musique : Funkwhale ![20%](images/funkwhale.png)
- vidéo : Peertube ![180%](images/peertube.svg)
- images : ?? (*@dansup@mastodon.social*)
- blogs : Plume ![10%](images/plume.svg) ou plugin Wordpress
- agregation plateformes centralisées : Vefto![2.5%](images/vefto.svg) 