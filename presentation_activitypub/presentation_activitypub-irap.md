<!-- $theme: gaia -->

# ActivityPub
# Ou comment fédérer les services web ?

---

## 1. Historique

Problèmes des médias sociaux centralisés (Facebook, Twitter, etc.) :

- **bruit** : publicités ciblées, contenus sponsorisés ;
- **filtrage** : censure, classement fermé ;
- **manipulation** : classement fermé, interface.


<!-- page_number: true -->

---

## Besoin d'alternatives

En qui faire confiance ?

![center](images/reseaux-centralises.svg)

---

## 2. Un réseau décentralisé

![center 15%](images/reseau-ap-1.svg)

---

## 2. Un réseau décentralisé

![center 15%](images/reseau-ap-2.svg)

---

## 2. Un réseau décentralisé

![center 15%](images/reseau-ap-3.svg)

---

## 2. Un réseau décentralisé

![center 15%](images/reseau-ap-4.svg)

<!-- Chacun peut s'approprier le langage comme il veut -->
<!-- demo ! -->

---

PubSubHubHub, Diaspora, etc...

![center 130%](images/standards.png)


![80%](images/logo-ap.png) - - - ![50%](images/logo-w3c.png)

---

### Quelques projets fédérés

- micro-blogging : Mastodon ![20%](images/mastodon.svg) ou Pleroma
- musique : Funkwhale ![20%](images/funkwhale.png)
- vidéo : Peertube ![180%](images/peertube.svg)
- images : PixelFed ![10%](images/pixelfed.png)
- blogs : Plume ![10%](images/plume.svg) ou plugin Wordpress
- agregation plateformes centralisées : Vefto![2.5%](images/vefto.svg)

---

![](images/all.png)

---

## Et dans la recherche ?

- Code
- Articles de recherche
- Astronomie

---

# Merci !

![](images/masto.png)

**Des questions ?**

Un article de vulgarisation : https://framablog.org/2018/05/23/comment-reparer-les-medias-sociaux-et-faire-encore-mieux/
