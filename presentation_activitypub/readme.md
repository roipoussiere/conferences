# Présentation d'ActivityPub

![](./header_article_light.png)

## Comment réparer les médias sociaux

THSF n°9 - jeudi 10 mai 2018

[![download pdf](https://img.shields.io/badge/pdf-download-blue.svg)](https://framagit.org/roipoussiere/conferences/raw/master/presentation_activitypub/presentation_activitypub-thsf.pdf?inline=false)
[![view source](https://img.shields.io/badge/source-view-orange.svg)](./presentation_activitypub-thsf.md)
[![event page](https://img.shields.io/badge/event page-go-green.svg)](https://www.thsf.net/talks.html#nathanael_jourdane)

*Tout allait bien au pays des réseaux sociaux, où les grandes start-ups du digital abrutissaient paisiblement leurs utilisateurs avec des publicités ciblées. Soudain, une sorte d'Espéranto d'Internet permis à des alternatives insignifiantes de se fédérer entre elles. La suite va vous étonner !*

## ActivityPub, où comment fédérer les services web

Présentation groupe gt2i de l'IRAP - vendredi 15 juin 2018

[![download pdf](https://img.shields.io/badge/pdf-download-blue.svg)](https://framagit.org/roipoussiere/conferences/raw/master/presentation_activitypub/presentation_activitypub-irap.pdf)
[![view source](https://img.shields.io/badge/source-view-orange.svg)](./presentation_activitypub-irap.md)

*ActivityPub est un protocole permettant d'interconnecter des application web. Il a été créé dans l'objectif de faciliter le développement d'alternatives aux services centralisés tels que Facebook ou Twitter.*

*Les logiciels utilisant ce protocole sont généralement open-source et peuvent donc être installés sur de nombreux serveurs, qu'on appelle instances. Celles-ci peuvent se suivre entre elles et les utilisateurs de différentes instances peuvent communiquer entre eux de manière transparente, comme s'ils étaient tous sur un seul gros serveur.*

*Après plusieurs années d'élaboration, ActivityPub a été standardisé par le W3C en ce début d'année. De nombreux logiciels l'implémentant commencent à voir le jour et certains logiciels existants tels que GitLab commencent à s'y intéresser.*
