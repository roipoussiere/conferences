### Construire un Internet éthique

INSA (organisé par le Club’Info et Ingénieurs Pour Demain) - mercredi 5 novembre 2018

[![download pdf](https://img.shields.io/badge/pdf-download-blue.svg)](https://framagit.org/roipoussiere/conferences/raw/master/internet_ethique-insa/internet_ethique-insa.pdf?inline=false)
[![view source](https://img.shields.io/badge/source-view-orange.svg)](./internet_ethique-insa.md)

Conférence co-réalisée avec Numahell

![](./affiche_light.png) ![](./conf_light.jpg)

*Nombreux sont les problèmes posés par la manière dont nous utilisons Internet aujourd’hui. Utilisation des données personnelles, problèmes posés par les GAFAs, dangers de la centralisation d’Internet et bien d’autres sujets seront présentés par l’association d’éducation populaire Framasoft, qui se consacre au logiciel libre depuis déjà plus de 10 ans.*

*C’est à nous tous de construire l’Internet de demain. Tu souhaites en apprendre plus sur ces problématiques et sur les alternatives existantes ? Rejoins-nous en amphi Fourier mercredi 5 novembre à 19h ! Au programme : une présentation réalisée par Framasoft, des questions-réponses, et une collation !*
