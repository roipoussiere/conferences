<!-- Please download Marp https://yhatt.github.io/marp/ to generate pdf document. -->

<style>
.slide {
  background-image: url("images/fond.jpg") !important;
  background-size: cover;
}
.slide h4 {
  color: gray;
  font-size: x-large !important;
  font-style: italic;
}
.slide h5 {
  text-align: center !important;
}
</style>

<!-- $theme: gaia -->
<!-- $size: 16:9 -->
<!-- footer: Emmanuelle et Nathanaël, pour Framasoft - INSA, 5 décembre 2018 -->

# Construire un Internet éthique

## INSA, 5 décembre 2018

##### Emmanuelle et Nathanaël (Framasoft)

----
<!-- page_number: true -->

#### Qui sommes-nous ?

## Framasoft

![center](images/logo_frama.png)

##### Association d'éducation populaire
##### et de promotion du logiciel libre et de la culture libre

---

## Construire un Internet éthique

##### 1. Les enjeux d'Internet
##### 2. Un autre modèle est-il possible ?
##### 3. Comment faire ?

----

# 1. Les enjeux d'Internet

----

#### 1. Les enjeux d'Internet
## Internet est partout

- dans nos habitudes de vies ;
- dans nos études et nos métiers ;
- dans nos démarches administratives.

![center 70%](images/montre.jpg)

---

#### 1. Les enjeux d'Internet ·
## Un monde de monopoles

Google, Amazon, Facebook, Apple, Microsoft, Uber, Airbnb, Netflix, …

![center](images/gafam.png)

----

#### 1. Les enjeux d'Internet ·
## L'économie de l'attention

- votre temps de cerveau disponible vaut de l'or
- ergonomie conçue pour vous faire rester
- publicité et profilage des données

![95%](images/autoplay.png) ![95%](images/blackmirror.jpg)


----

#### 1. Les enjeux d'Internet
## La transformation du travail

Exploitation des travailleur⋅e⋅s à tous niveaux : fabrication, distribution, recyclage, modération de contenus, plateformes…

![105% center](images/deliveroo.jpg)

----

#### 1. Les enjeux d'Internet
## Impact écologique

- extraction des Terres rares
- consommation d'énergie
- déchets électroniques

![center 80%](images/dechets.jpg)

----

#### 1. Les enjeux d'Internet
## Exclusion numérique

- Équipement ou connexion dégradée
- Manque de connaissances
- Usages différenciés : créatifs pour les uns, passifs pour les autres

![center 60%](images/echap.png)

----

#### 1. Les enjeux d'Internet
## Pour se remonter le moral…

![center 200%](images/chaton.gif)

---

# 2. Un autre modèle est-il possible ?

----

#### 2. Un autre modèle est-il possible ? ·
## Le logiciel libre

##### Utiliser
##### étudier
##### distribuer
##### copier

![center 90%](images/fsfs.png)

----

#### 2. Un autre modèle est-il possible ? ·
## Décentralisation

- répartition des coûts ;
- répartition de la gouvernance ;
- des infrastructures à taille humaine ;
- auto-hébergement

![90%](images/centralise.png) ![90%](images/decentralise.png)

----

#### 2. Un autre modèle est-il possible ?
## Culture libre, savoirs ouverts

Au-delà du logiciel, les Creative Commons : partager l'art et les savoirs

- Réutilisation à condition
- Partage sous les mêmes conditions ou sans contrainte
- Modification autorisée ou non
- Utilisation commerciale ou non

----

#### 2. Un autre modèle est-il possible ?
## Les communs, diversité des modèles d'organisation

S'organiser autour d'une ressource pour la partager et la rendre pérenne.

Cf. les travaux d'Elinor Ostrom

----

![center 105%](images/les_communs.png)
<!-- Par Pierre Trendel, CC BY-SA 4.0 -->

----

#### 2. Un autre modèle est-il possible ?
## Autres modes d'organisation et de financement

Explorer les modèles de l'Économie solidaire et sociale

- Sortir du financement par la publicité : financement par les dons, parts sociales, épargne sociale
- Modes d'organisation : mutualités, coopératives, fédérations…

----

## Tout cela est bien beau mais…

![center 88%](images/chaton.jpg)

---

# 3. Comment faire ?

----

#### 3. Comment faire ? ·
## Utiliser

- l'annuaire du libre (framalibre.org)
- 34 services en ligne par Framasoft (degooglisons-internet.org)
- chercher des alternatives (alternativeto.net)

![center 92%](images/degooglisons.jpg)

----

#### 3. Comment faire ? ·
## Se rencontrer

Des collectifs et associations du libre

- [CHATONS.org](http://chatons.org/), hébergeurs alternatifs
- [FFDN](http://ffdn.org/), fournisseurs d'accès internet
- [Associations locales pour le logiciel libre](https://www.agendadulibre.org/orgas)

Les événements du libre sur l'[agenda du libre](http://agendadulibre.org/)

----

#### 3. Comment faire ?
## Accompagner

Là où vous vous impliquez déjà, mettre à disposition des outils libres adaptés aux besoins.

![center 31%](images/install_party.jpg)

----

#### 3. Comment faire ?
## Étudier, contribuer

Stages et projets tutorés sur des projets libres ?

Exemples:

- [Université Technologique de Compiègne](https://framablog.org/2018/07/07/contribuer-a-un-logiciel-libre-dans-une-formation-en-ecole-dingenieur/)
- [Université de Fribourg](https://www.youtube.com/watch?v=kHrCrf_Gyy8)
- INSA Lyon
- etc.

----

#### 3. Comment faire ? ·
## En tant que futur⋅e ingénieur⋅e

##### *Ce projet ou cette entreprise a-t'elle des valeurs qui me correspond ?*

##### *Comment concevoir cette nouvelle applications de manière éthique ?*

---

# Questions ?

----

# Merci !