# Téléphones libres

Présenté lors des différents Ateliers Téléphones Libres à Toulouse.

[![download pdf](https://img.shields.io/badge/pdf-download-blue.svg)](https://framagit.org/roipoussiere/conferences/raw/master/telephones_libres/telephones_libres.pdf?inline=false)
[![view source](https://img.shields.io/badge/source-view-orange.svg)](./telephones_libres.md)
[![event page](https://img.shields.io/badge/event%20page-go-green.svg)](https://telephoneslibres.frama.site/)

![](./atelier_light.jpg)

*Les smartphones sont des outils très pratiques, mais ont tendance à ne pas vraiment respecter notre vie privée. Véritables ordinateurs de poche bourrés de capteurs, on peut les considérer comme de véritables mouchards ambulants.*

*Dès l'achat, votre téléphone embarque des applications que vous n'avez pas choisi : c'est le résultat de partenariats entre développeurs d'applications, fabricants et vendeurs de téléphones (on appelle cela la vente liée).*

*Celles-ci récupèrent souvent vos données personnelles (position géographique, liste de contacts ou habitudes d'utilisation) qui sont par la suite utilisées par exemple pour du ciblage publicitaire.*

*Toutefois, des solutions existent :*

- *configurer certaines applications, comme votre navigateur web, pour limiter l'envoi de données personnelles ;*
- *remplacer GooglePlay par un autre magasin d'applications proposant des applications plus éthiques ;*
- *remplacer le système Google Android par une alternative plus respectueuse de votre vie privée.*

*Tout cela est un peu compliqué : c'est pourquoi des ateliers existent pour faciliter l'entraide entre débutants et habitués.*
