<!-- $theme: gaia -->

Atelier installation d'alternatives à Google Android
----------------------------------------------------

![70%](images/bulldozer.svg) ![84%](images/google-android.png)

*Toulouse - 14/02/2018 :heart:*

---
<!-- page_number: true -->

Un constat
----------

- les smartphones ça peut faire pas mal de choses
- on les a toujours sur nous.

![center 90%](images/fallout.png)

---

Petit historique
----------------

- ![2.8%](images/pacman.png) 2005 : Google rachète *Android Inc.*
- ![10%](images/android.png) octobre 2008 : sortie du 1er téléphone Android
- :tada: peu après : on découvre comment modifier l'OS
- ![11%](images/cmod.png) après quelques bidouillages -> CyanogenMod
- :busts_in_silhouette: une communauté se crée autour du projet
- :moneybag: création de *Cyanogen Inc.*
- ![8%](images/fork.png) d'autres distributions sortent...
- :dizzy_face: décembre 2016 : arrêt de CyanogenMod
- ![49%](images/linos.png) janvier 2017 : publication de LineageOS

---

Où se cache Google ?
--------------------

![center 75%](images/marketshare.svg)

---

Les Google Apps, l'écosystème G.
-------------------

![115%](images/gaps.jpg)

- applis intégrées par défaut, non désintallables
- écosystème attractif, mais fermé
- objectif : vous faire utiliser Google au quotidien

---

Le compte Google
---

![55%](images/google-account.jpg) ![68%](images/google-location-history.png)

- :arrow_right: nous identifier en tant qu'individu
- :arrow_right: croiser les infos avec les compte existant

---

Quand tout le monde s'y met
---------------------------

- Google impose son écosystème propriétaire ;
- les contructeurs ajoutent leur touche perso ;
- les opérateurs aussi.

![60%](images/galaxy-s8.jpg) ![57%](images/vente-liee.png)

- :handshake: contrats à tout va pour amortir le prix du tél.
- :arrow_right: un tél. truffé d'applis proprio et de mouchards.

---

Solution : tout remettre à zero
------------------------------

![70%](images/bulldozer.svg) ![84%](images/google-android.png)

... et remplacer par un système (à peu près) libre : ![center 20%](lineageos.png)

---

LineageOS ne résout pas tout...
-------------------------------

- matériel et drivers propriétaires
	- :arrow_right: ¯\\_(ツ)_/¯
- GSM : SMS, méta-données, triangulation, ...
	- :arrow_right: Signal ![20%](images/sign.png) ; mode avion ![20%](images/airplane.png)
- Adresse IP ; analyse trafic internet
	- :arrow_right: VPN ![17%](images/open-vpn.png) ; Tor ![19%](images/tor.png)
- autres applications propriétaires
	- :arrow_right: f-droid ![30%](images/fdroid.png) ; Exodus Privacy ![5%](images/exodus.svg)